# [3.1.0](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/charts/opendesk-jitsi/compare/v3.0.1...v3.1.0) (2025-03-05)


### Bug Fixes

* **files:** generate lang files from templates ([0d48d40](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/charts/opendesk-jitsi/commit/0d48d40ba500edf4d737023182c4d6a9903d6a7e))


### Features

* **dialin:** Enable the invite button ([2a8dbe3](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/charts/opendesk-jitsi/commit/2a8dbe38f2ce08a43f3b85f4ddca2f7790f0edba))
* **dialin:** Set related env and generated related config files ([e897be8](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/charts/opendesk-jitsi/commit/e897be8ccd708a769565a33d82f42f2156cfdb79))
* **images:** Add Jigasi into values.yaml ([a3fda34](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/charts/opendesk-jitsi/commit/a3fda348c0ef6eae192367064a769acc220abd37))
* **images:** Upgrade jitsi-keycloak-adapter for Deno 2.1.6 ([9857aff](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/charts/opendesk-jitsi/commit/9857aff87d4a4297c9a6d4cf367f52f3788df360))

## [3.0.1](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/charts/opendesk-jitsi/compare/v3.0.0...v3.0.1) (2024-12-20)


### Bug Fixes

* Set title string without hardcoded value ([d86b149](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/charts/opendesk-jitsi/commit/d86b149338f762a1ead7f9472b7b0e027226e7e6))

# [3.0.0](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/charts/opendesk-jitsi/compare/v2.1.1...v3.0.0) (2024-12-20)


### Bug Fixes

* Reuse compliance ([f01b632](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/charts/opendesk-jitsi/commit/f01b6321d667858f23b05f785a400fc91f8ce165))


### Features

* Support to set HTML page title ([678d734](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/charts/opendesk-jitsi/commit/678d734efe0b241b9f67f43e8a7e895559a9c566))


### BREAKING CHANGES

* Removed option `theme.texts.productName` and added `theme.title`.

## [2.1.1](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/charts/opendesk-jitsi/compare/v2.1.0...v2.1.1) (2024-11-27)


### Bug Fixes

* **files:** do not set a border color for the room list if it is not enabled ([d457e7f](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/charts/opendesk-jitsi/commit/d457e7f0557803f9da08f05a3b3adf40d70b1f49))
* **files:** format plugin.head.html ([8669077](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/charts/opendesk-jitsi/commit/8669077b4d927993d21bd9eb49b484efc7d6b096))

# [2.1.0](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/charts/opendesk-jitsi/compare/v2.0.3...v2.1.0) (2024-11-18)


### Features

* **files:** switch hangup buttons colors in the plugin.head.html ([7924aaa](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/charts/opendesk-jitsi/commit/7924aaaf1f3bf1c5996b13518d022675b7c934ca))

## [2.0.3](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/charts/opendesk-jitsi/compare/v2.0.2...v2.0.3) (2024-11-13)


### Bug Fixes

* Ensure jitsi-jvb is restarted as well. ([fbeeed3](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/charts/opendesk-jitsi/commit/fbeeed33ab00a69f8dc1304fa18b2f9bff4785cc))

## [2.0.2](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/charts/opendesk-jitsi/compare/v2.0.1...v2.0.2) (2024-11-12)


### Bug Fixes

* **charts:** use nubus as hostname instead of univentionCorporateServer ([6963e3e](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/charts/opendesk-jitsi/commit/6963e3e12451c59687661765a13c326b38771129))
* **files:** format body.html ([871d0ad](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/charts/opendesk-jitsi/commit/871d0ad65b240362e02151c196931ac0f202f52b))
* **files:** format custom-config.js ([f4faf1e](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/charts/opendesk-jitsi/commit/f4faf1e284a78ee7c7bbfb0f4bd33818d8fa787d))
* **files:** format custom-interface_config.js ([46c6a86](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/charts/opendesk-jitsi/commit/46c6a86e85d09dbaa4dab926c796d3d884af63dc))
* **files:** format oidc-adapter.html ([09b7835](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/charts/opendesk-jitsi/commit/09b78353084c73a7b8d729f403ab8e861e229440))
* **files:** format plugin.head.html ([cb2de7e](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/charts/opendesk-jitsi/commit/cb2de7e5593076ae32239e038b9b70f483643356))
* **files:** remove the blank lines from the beginning of SVGs which breaks the file format ([0c2a067](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/charts/opendesk-jitsi/commit/0c2a067e2703631df32f8a6240552171e216fdb4))
* **files:** update the file permission of welcome-background.jpg ([739dcd5](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/charts/opendesk-jitsi/commit/739dcd59fa7723e5cb52432a78ce68ec97fd80a8))
* **jitsi-web:** Restart on deployment to ensure changes are picked up. ([ef286cd](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/charts/opendesk-jitsi/commit/ef286cd57fa08c73779ba7a76aac6fed37e62278))
* patchJVB script updated for compatibility with updated Helm chart. ([792c9e9](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/charts/opendesk-jitsi/commit/792c9e9a6c0ed131501eeffc13d4e31789360060))
* Remove streaming configuration. ([e0cbb77](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/charts/opendesk-jitsi/commit/e0cbb7723b8c11c9bbc9616fd2fd11a93b1664b1))
* Template `interfaceConfig.RECENT_LIST_ENABLED` based on `.Values.jitsi.web.extraConfigJs.doNotStoreRoom`. ([23a4793](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/charts/opendesk-jitsi/commit/23a47933aa423abcadca255bb82d1ddad3f95513))
* **UI:** disable the moderator notification which popups a wrong message in guest case ([c63493f](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/charts/opendesk-jitsi/commit/c63493fad2d29bda010229c359da963004c264b2))
* **UI:** use the new toolbarButtons property ([9ad37f4](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/charts/opendesk-jitsi/commit/9ad37f4afe12cbfe7ed04808727a92e564352831))

## [2.0.1](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/charts/opendesk-jitsi/compare/v2.0.0...v2.0.1) (2024-11-07)


### Bug Fixes

* Remove minus in license header comment start `{{-`. ([6ced6e4](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/charts/opendesk-jitsi/commit/6ced6e4f1dd3a0d82a0252a70f4d38415ef8c8d5))

# [2.0.0](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/charts/opendesk-jitsi/compare/v1.12.5...v2.0.0) (2024-11-06)


### Features

* **opendesk-jitsi-keycloak-adapter:** Added support for secrets ([fed0942](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/charts/opendesk-jitsi/commit/fed0942fb726b3cd1b764cc51a13715a6a46b9d0))


### BREAKING CHANGES

* **opendesk-jitsi-keycloak-adapter:** Environment variable JWT_APP_SECRET is now propagated with the help of a
Kubernetes secret instead of a plain value

## [1.12.5](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/charts/opendesk-jitsi/compare/v1.12.4...v1.12.5) (2024-10-08)


### Bug Fixes

* Indentation in configmap. ([6142f45](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/charts/opendesk-jitsi/commit/6142f45b1ead46d11e2288717b6528f2f4a5321c))

## [1.12.4](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/charts/opendesk-jitsi/compare/v1.12.3...v1.12.4) (2024-10-08)


### Bug Fixes

* Name of template variable for portal's hostname. ([eadecc5](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/charts/opendesk-jitsi/commit/eadecc58fe36b2f8039e0dbf1106f7155dbbd90f))

## [1.12.3](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/charts/opendesk-jitsi/compare/v1.12.2...v1.12.3) (2024-10-07)


### Bug Fixes

* **opendesk-jitsi:** Fix typo in configmap jitsi-meet-binaries ([8323697](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/charts/opendesk-jitsi/commit/8323697770e142714e813e6f01a43ed8161285d7))

## [1.12.2](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/charts/opendesk-jitsi/compare/v1.12.1...v1.12.2) (2024-10-07)


### Bug Fixes

* **opendesk-jitsi:** Change header backgroup to jpg ([9c570d7](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/charts/opendesk-jitsi/commit/9c570d79c55cd8af654b24781e6ffd258ce4c047))

## [1.12.1](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/charts/opendesk-jitsi/compare/v1.12.0...v1.12.1) (2024-09-24)


### Bug Fixes

* **theme:** Get colors from values.yaml ([8db91fa](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/charts/opendesk-jitsi/commit/8db91fa37791173ccfad03fe441843e02b9be032))

# [1.12.0](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/charts/opendesk-jitsi/compare/v1.11.3...v1.12.0) (2024-09-23)


### Bug Fixes

* **branding:** Keep comments consistent ([32a0e55](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/charts/opendesk-jitsi/commit/32a0e5560a11e8e541095db8891e9a81bfc9081a))
* **branding:** Update overwritten labels for German ([f47d809](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/charts/opendesk-jitsi/commit/f47d80905a41b61d612d9b9d7206f5533b2641f4))
* **config.js:** doNotStoreRoom as boolean ([28d1a1e](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/charts/opendesk-jitsi/commit/28d1a1e992752cf81a8b43fb7acfef60d98759f6))
* **jitsi-keycloak-adapter:** remove duplicated word in the name definition ([c7f3617](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/charts/opendesk-jitsi/commit/c7f36174beb59281bcdcf7d55d2b81d0b5d92f95))
* **jitsi-keycloak-adapter:** use userInfo.name as displayname in tokens ([97bc274](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/charts/opendesk-jitsi/commit/97bc274cc3a1793d638db5d5e7641f2557336924))
* **language:** Fix typo ([1cf8c0b](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/charts/opendesk-jitsi/commit/1cf8c0b59cc2ebc4730bdabfe064fb6292ec88d5))
* **UI:** Allow the room list in the welcome page ([1508b1b](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/charts/opendesk-jitsi/commit/1508b1b7c4d8a975049976aeffea4913d382e7ef))
* **UI:** Titles in the welcome page ([b8bce5e](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/charts/opendesk-jitsi/commit/b8bce5e4e96b62478edd7c5f4f592dd578a3f02a))
* **UI:** Update the internal button color ([977d4cb](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/charts/opendesk-jitsi/commit/977d4cb67774ff2d314edfb0718f763ca084f919))


### Features

* **UI:** add the backgound image ([d106068](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/charts/opendesk-jitsi/commit/d1060683ba52abb84fcd980952b158e92664e082))

## [1.11.3](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/charts/opendesk-jitsi/compare/v1.11.2...v1.11.3) (2024-09-18)


### Bug Fixes

* **menu:** fix the position and the visibility of the opendesk menu ([d1c0247](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/charts/opendesk-jitsi/commit/d1c024723f9dbf801516bc94f494f87a1f2ca3d2))

## [1.11.2](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/charts/opendesk-jitsi/compare/v1.11.1...v1.11.2) (2024-09-17)


### Bug Fixes

* **theme:** Watermark in conference. ([fe401d4](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/charts/opendesk-jitsi/commit/fe401d4aacaf0a688823ab91f85b43fde176eada))

## [1.11.1](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/charts/opendesk-jitsi/compare/v1.11.0...v1.11.1) (2024-09-17)


### Bug Fixes

* **branding:** get the logo click link from template ([032cee0](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/charts/opendesk-jitsi/commit/032cee038ea53d8a0aa75f6f2868655530155a9b))
* **logo:** Add the target link for logo click ([3e834d3](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/charts/opendesk-jitsi/commit/3e834d3f0329fb810dfb167b32ee42f13dccbd0d))
* **topbar:** fix scrolling ([fccced8](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/charts/opendesk-jitsi/commit/fccced885be26e1bc42e191ac8043a62db5f6274))
* **topbar:** navigation.json inside iframe to refresh the session after the silent login ([cc8b497](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/charts/opendesk-jitsi/commit/cc8b497754ea51cd05ac6d22c8eb5268f0cf8ac8))
* **welcome page:** no footer, recommends room names, enable the recent list ([01cfc33](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/charts/opendesk-jitsi/commit/01cfc33b35ae981bf888dab086886f6d84fa9841))

# [1.11.0](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/charts/opendesk-jitsi/compare/v1.10.0...v1.11.0) (2024-09-16)


### Features

* **brabding.json:** add the basic branding ([9e8bb89](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/charts/opendesk-jitsi/commit/9e8bb89ec5c6202f156deea7842c6e0c3529ca41))

# [1.10.0](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/charts/opendesk-jitsi/compare/v1.9.3...v1.10.0) (2024-09-16)


### Bug Fixes

* **body.html:** no topbar during the meeting session. It breaks UI ([c583c92](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/charts/opendesk-jitsi/commit/c583c921ced0a768f036df92610b5dede7151bdf))
* **body.html:** silent login through ICS ([2cd6823](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/charts/opendesk-jitsi/commit/2cd68238d663d33d4bea010c6506f43a394d69d5))
* **ics:** add ICS related parameters and configs ([b0ad493](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/charts/opendesk-jitsi/commit/b0ad493c6a68288d189c88f47c56a8de56cdb38c))


### Features

* **body.html:** Add OpenDesk menu to Jitsi ([8d37ac5](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/charts/opendesk-jitsi/commit/8d37ac5d5bfcc6d87f4c785027dbf2cc25b324fa))
* **portal-url:** provide the portal url for Jitsi frontend ([d81a47b](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/charts/opendesk-jitsi/commit/d81a47b5646fe8790b6ea7c5c4a41b05db63bdb5))

## [1.9.3](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/charts/opendesk-jitsi/compare/v1.9.2...v1.9.3) (2024-09-02)


### Bug Fixes

* **context.ts:** fail if there is no valid opendesk_username ([9ebb7f4](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/charts/opendesk-jitsi/commit/9ebb7f4deacfe5043816092ade5bd8ea11efd8fe))

## [1.9.2](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/charts/opendesk-jitsi/compare/v1.9.1...v1.9.2) (2024-08-09)


### Bug Fixes

* **context.ts:** Return `null` when a required OIDC claim is not available. ([a55da5b](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/charts/opendesk-jitsi/commit/a55da5b9ba0efc56f236541c5d8bf50a58f3db01))

## [1.9.1](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/charts/opendesk-jitsi/compare/v1.9.0...v1.9.1) (2024-08-09)


### Bug Fixes

* **opendesk-jitsi:** set COLIBRI_WEBSOCKET_REGEX explicitly. Otherwise unsafe regex will be applied by default ([a8eb673](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/charts/opendesk-jitsi/commit/a8eb673f827cf8f3b8f0e071f3476df6558d4c23))
* **opendesk-jitsi:** update the customized Nginx template based on the current upstream ([c2ab80b](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/charts/opendesk-jitsi/commit/c2ab80bf1f310e72ba788559577f17ca0e527848))

# [1.9.0](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/charts/opendesk-jitsi/compare/v1.8.0...v1.9.0) (2024-08-06)


### Features

* **opendesk-jitsi:** update tags to use the latest stable images ([284f2c5](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/charts/opendesk-jitsi/commit/284f2c5f1288a2a166b375bdb971c39111b0ae0d))

# [1.8.0](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/charts/opendesk-jitsi/compare/v1.7.9...v1.8.0) (2024-07-02)


### Features

* **opendesk-jitsi:** Add extraVolumes and extraVolumeMounts option ([5aab2a2](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/charts/opendesk-jitsi/commit/5aab2a2501907c94a078ff0a3aa6e0492729d8a4))

## [1.7.9](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/charts/opendesk-jitsi/compare/v1.7.8...v1.7.9) (2024-05-17)


### Bug Fixes

* **jibri:** *BREAKING* Bump to upstream Helm chart 1.3.8 and set /dev/shm size to 2Gi as required by the Jibri image. Requires manual deletion of prosody StatefulSet see https://github.com/jitsi-contrib/jitsi-helm/releases/tag/v1.3.8 ([b9f6ab1](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/charts/opendesk-jitsi/commit/b9f6ab11f6d0309e528df216885e21898b8dd1e4))

## [1.7.8](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/charts/opendesk-jitsi/compare/v1.7.7...v1.7.8) (2024-01-22)


### Bug Fixes

* **opendesk-jitsi:** Add missing imagePullPolicy template ([be2f322](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/charts/opendesk-jitsi/commit/be2f322520a8b55ca0c5f406939393bc836257dd))

## [1.7.7](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/charts/opendesk-jitsi/compare/v1.7.6...v1.7.7) (2024-01-17)


### Bug Fixes

* **helmchart:** Run patchJVB only if enabled ([ee8d20d](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/charts/opendesk-jitsi/commit/ee8d20da3a9984039965c25f2ba9191b91e73ad1))

## [1.7.6](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/charts/opendesk-jitsi/compare/v1.7.5...v1.7.6) (2024-01-16)


### Bug Fixes

* **yaml:** Satisfy yaml linter ([0df16d3](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/charts/opendesk-jitsi/commit/0df16d3eb4bf8181317d8f11f819a98af3089c0c))

## [1.7.5](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/charts/opendesk-jitsi/compare/v1.7.4...v1.7.5) (2023-12-21)


### Bug Fixes

* Add GPG key, update README.md ([d482642](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/charts/opendesk-jitsi/commit/d4826420797a7ef44c08302ddfd724c45e013524))

## [1.7.4](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/charts/opendesk-jitsi/compare/v1.7.3...v1.7.4) (2023-12-21)


### Bug Fixes

* **helmchart:** Rename to opendesk-jitsi !BREAKING ([37a67fa](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/charts/opendesk-jitsi/commit/37a67fa67493defdd404d3d77a3eed5d11c154d1))

## [1.7.3](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/charts/opendesk-jitsi/compare/v1.7.2...v1.7.3) (2023-12-20)


### Bug Fixes

* **ci:** Move repo to Open CoDE ([80c2bd9](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/charts/opendesk-jitsi/commit/80c2bd9066c985ab3a78b0201bd96dd91c3fdfcc))

## [1.7.2](https://gitlab.souvap-univention.de/souvap/tooling/charts/sovereign-workplace-jitsi/compare/v1.7.1...v1.7.2) (2023-12-11)


### Bug Fixes

* **sovereign-workplace-jitsi:** JwtAppId is not optional. Its value is always the meeting domain ([8b2897a](https://gitlab.souvap-univention.de/souvap/tooling/charts/sovereign-workplace-jitsi/commit/8b2897aacb1662e7d76d8dd4381d843fae7c53fe))

## [1.7.1](https://gitlab.souvap-univention.de/souvap/tooling/charts/sovereign-workplace-jitsi/compare/v1.7.0...v1.7.1) (2023-10-25)


### Bug Fixes

* **sovereign-workplace-jitsi:** Use "| quote" wherever possible ([82328d5](https://gitlab.souvap-univention.de/souvap/tooling/charts/sovereign-workplace-jitsi/commit/82328d53baea9463c7447db7e43473cf7101695e))

# [1.7.0](https://gitlab.souvap-univention.de/souvap/tooling/charts/sovereign-workplace-jitsi/compare/v1.6.0...v1.7.0) (2023-10-19)


### Features

* **sovereign-workplace-jitsi:** Custom Jitsi UI for Element Jitsi Widget ([20719b9](https://gitlab.souvap-univention.de/souvap/tooling/charts/sovereign-workplace-jitsi/commit/20719b93fd929e2e37d622c92eccf4a09a780085))

# [1.6.0](https://gitlab.souvap-univention.de/souvap/tooling/charts/sovereign-workplace-jitsi/compare/v1.5.1...v1.6.0) (2023-10-17)


### Features

* **sovereign-workplace-jitsi:** Enable internal chat ([4057539](https://gitlab.souvap-univention.de/souvap/tooling/charts/sovereign-workplace-jitsi/commit/40575397adaa0d1e0fac34bdfaa4dd89b77a559d))

## [1.5.1](https://gitlab.souvap-univention.de/souvap/tooling/charts/sovereign-workplace-jitsi/compare/v1.5.0...v1.5.1) (2023-09-06)


### Bug Fixes

* **sovereign-workplace-jitsi:** Remove configmaps and volumes for custom prosody files ([6da870a](https://gitlab.souvap-univention.de/souvap/tooling/charts/sovereign-workplace-jitsi/commit/6da870ab222c91fe6078e6acd45aef77ed4e0f46))
* **sovereign-workplace-jitsi:** Remove custom prosody files ([e6d72c7](https://gitlab.souvap-univention.de/souvap/tooling/charts/sovereign-workplace-jitsi/commit/e6d72c7225ad041b7c1a86f8490e935d03dd1615))
* **sovereign-workplace-jitsi:** Remove duplicated volume lines ([3a5274c](https://gitlab.souvap-univention.de/souvap/tooling/charts/sovereign-workplace-jitsi/commit/3a5274c6fbd26aa082e197cc022ad89e62d70751))

# [1.5.0](https://gitlab.souvap-univention.de/souvap/tooling/charts/sovereign-workplace-jitsi/compare/v1.4.2...v1.5.0) (2023-09-06)


### Bug Fixes

* **sovereign-workplace-jitsi:** Update version to allow read-only filesystem ([3907788](https://gitlab.souvap-univention.de/souvap/tooling/charts/sovereign-workplace-jitsi/commit/390778886feeb637fd4347033e6dc65a03873e75))


### Features

* **sovereign-workplace-jitsi:** Add Readiness and Liveness probes to keycloak adapter ([ac20347](https://gitlab.souvap-univention.de/souvap/tooling/charts/sovereign-workplace-jitsi/commit/ac2034750031e6dcac59a71c918f2ded8370b821))

## [1.4.2](https://gitlab.souvap-univention.de/souvap/tooling/charts/sovereign-workplace-jitsi/compare/v1.4.1...v1.4.2) (2023-09-05)


### Bug Fixes

* **sovereign-workplace-jitsi:** Improve default security settings ([82cd3d8](https://gitlab.souvap-univention.de/souvap/tooling/charts/sovereign-workplace-jitsi/commit/82cd3d8297b9601dcf97d45e96b1cdb623427955))

## [1.4.1](https://gitlab.souvap-univention.de/souvap/tooling/charts/sovereign-workplace-jitsi/compare/v1.4.0...v1.4.1) (2023-08-30)


### Bug Fixes

* **sovereign-workplace-jitsi:** Use userInfo.name as Jitsi profile ([a56b2c2](https://gitlab.souvap-univention.de/souvap/tooling/charts/sovereign-workplace-jitsi/commit/a56b2c25f2b921bd0e36d0792ae76ee8b4a58b57))

# [1.4.0](https://gitlab.souvap-univention.de/souvap/tooling/charts/sovereign-workplace-jitsi/compare/v1.3.0...v1.4.0) (2023-08-29)


### Features

* **sovereign-workplace-jitsi:** Upgrade jitsi-keycloak-adapter to support custom profile logic by using Keycloak's userinfo. ([840091c](https://gitlab.souvap-univention.de/souvap/tooling/charts/sovereign-workplace-jitsi/commit/840091cca21ca2c42400dde8d564d4df4c8e8d35))

# [1.3.0](https://gitlab.souvap-univention.de/souvap/tooling/charts/sovereign-workplace-jitsi/compare/v1.2.5...v1.3.0) (2023-08-29)


### Features

* **sovereign-workplace-jitsi:** Support for basic theming attributes ([56b99dd](https://gitlab.souvap-univention.de/souvap/tooling/charts/sovereign-workplace-jitsi/commit/56b99dd6f2e462a3d0f1601ca164b8e5edd4df0b))

## [1.2.5](https://gitlab.souvap-univention.de/souvap/tooling/charts/sovereign-workplace-jitsi/compare/v1.2.4...v1.2.5) (2023-08-16)


### Bug Fixes

* **sovereign-workplace-jitsi:** Fix limit check ([6c34051](https://gitlab.souvap-univention.de/souvap/tooling/charts/sovereign-workplace-jitsi/commit/6c3405119e25b3f63be3df116589f360638ed566))

## [1.2.4](https://gitlab.souvap-univention.de/souvap/tooling/charts/sovereign-workplace-jitsi/compare/v1.2.3...v1.2.4) (2023-08-16)


### Bug Fixes

* **sovereign-workplace-jitsi:** Update README with OCI installation method ([c2a872a](https://gitlab.souvap-univention.de/souvap/tooling/charts/sovereign-workplace-jitsi/commit/c2a872aa54122fd09925407096e17dc59e9cfe05))

## [1.2.4](https://gitlab.souvap-univention.de/souvap/tooling/charts/sovereign-workplace-jitsi/compare/v1.2.3...v1.2.4) (2023-08-16)


### Bug Fixes

* **sovereign-workplace-jitsi:** Update README with OCI installation method ([c2a872a](https://gitlab.souvap-univention.de/souvap/tooling/charts/sovereign-workplace-jitsi/commit/c2a872aa54122fd09925407096e17dc59e9cfe05))

## [1.2.3](https://gitlab.souvap-univention.de/souvap/tooling/charts/sovereign-workplace-jitsi/compare/v1.2.2...v1.2.3) (2023-08-16)


### Bug Fixes

* **sovereign-workplace-jitsi:** Fix patchJVB missing curly bracket ([22da7ef](https://gitlab.souvap-univention.de/souvap/tooling/charts/sovereign-workplace-jitsi/commit/22da7ef3f5c5fdc5e5447f9bd3ece35414984cb9))

## [1.2.2](https://gitlab.souvap-univention.de/souvap/tooling/charts/sovereign-workplace-jitsi/compare/v1.2.1...v1.2.2) (2023-08-16)


### Bug Fixes

* **sovereign-workplace-jitsi:** Fix patchJVB breaking loop and let status field be customized ([60186ee](https://gitlab.souvap-univention.de/souvap/tooling/charts/sovereign-workplace-jitsi/commit/60186ee1725fab6cf5ae1e1eb9a08463d540ee30))

## [1.2.1](https://gitlab.souvap-univention.de/souvap/tooling/charts/sovereign-workplace-jitsi/compare/v1.2.0...v1.2.1) (2023-08-14)


### Bug Fixes

* **sovereign-workplace-jitsi:** Fix nodePort deployment with different egress gateway ([cc79996](https://gitlab.souvap-univention.de/souvap/tooling/charts/sovereign-workplace-jitsi/commit/cc79996d0ed97d226cbda2012db916c0e2e88e7b))

# [1.2.0](https://gitlab.souvap-univention.de/souvap/tooling/charts/sovereign-workplace-jitsi/compare/v1.1.3...v1.2.0) (2023-08-14)


### Bug Fixes

* **sovereign-workplace-jitsi:** Add SPDX license/copyright identifier ([ba45ab0](https://gitlab.souvap-univention.de/souvap/tooling/charts/sovereign-workplace-jitsi/commit/ba45ab0b760b07eb498d8a3bb474fc19cb04e9fc))
* **sovereign-workplace-jitsi:** Add SPDX license/copyright identifier ([9ff37d1](https://gitlab.souvap-univention.de/souvap/tooling/charts/sovereign-workplace-jitsi/commit/9ff37d1b6302cd04a9a27fe4cd49e1f7a80748ac))
* **sovereign-workplace-jitsi:** Disable auto-login on jicofo ([cd582d1](https://gitlab.souvap-univention.de/souvap/tooling/charts/sovereign-workplace-jitsi/commit/cd582d1926cc651df82e8ed1e8a82c5388809872))
* **sovereign-workplace-jitsi:** Disable auto-login on jicofo ([0822928](https://gitlab.souvap-univention.de/souvap/tooling/charts/sovereign-workplace-jitsi/commit/082292873f14682066739695bda2b27a4db51e5f))
* **sovereign-workplace-jitsi:** Resolv conflicts ([88c0990](https://gitlab.souvap-univention.de/souvap/tooling/charts/sovereign-workplace-jitsi/commit/88c09904c82fc143dfea2449583d8375a48dd751))
* **sovereign-workplace-jitsi:** Set replicaCount to 1 for JVB. JVB cannot be scale by increasing replicaCount ([fdf0d7a](https://gitlab.souvap-univention.de/souvap/tooling/charts/sovereign-workplace-jitsi/commit/fdf0d7a780e42150345652a821cf4a90ef1d60d6))


### Features

* **sovereign-workplace-jitsi:** Add custom prosody config to support hybrid_matrix_token ([07654ae](https://gitlab.souvap-univention.de/souvap/tooling/charts/sovereign-workplace-jitsi/commit/07654ae5094d4f1d7e76b82652bb0608bb78334d))
* **sovereign-workplace-jitsi:** Add custom prosody config to support hybrid_matrix_token ([4beceec](https://gitlab.souvap-univention.de/souvap/tooling/charts/sovereign-workplace-jitsi/commit/4beceeccd16ea5469b18d6a0bc7c06b24ff9cdc5))
* **sovereign-workplace-jitsi:** Add custom prosody plugins into files ([f5a91e2](https://gitlab.souvap-univention.de/souvap/tooling/charts/sovereign-workplace-jitsi/commit/f5a91e27cff0f748f8835f17de7a3cb2da7d1a40))
* **sovereign-workplace-jitsi:** Add custom prosody plugins into files ([b5cf248](https://gitlab.souvap-univention.de/souvap/tooling/charts/sovereign-workplace-jitsi/commit/b5cf248bd7eafbd4baa1087e7d6054a98075fce8))
* **sovereign-workplace-jitsi:** Add extra volumes into values.yaml for Prosody ([f2acf70](https://gitlab.souvap-univention.de/souvap/tooling/charts/sovereign-workplace-jitsi/commit/f2acf702f74b7a2b667e1661027e174226d45098))
* **sovereign-workplace-jitsi:** Add JICOFO_AUTH_LIFETIME as Jicofo environment variable ([513af75](https://gitlab.souvap-univention.de/souvap/tooling/charts/sovereign-workplace-jitsi/commit/513af75a98bfce5746df2cd7014ac0837f57f65d))
* **sovereign-workplace-jitsi:** Create ConfigMaps for Prosody custom files ([6939a31](https://gitlab.souvap-univention.de/souvap/tooling/charts/sovereign-workplace-jitsi/commit/6939a316928e547d4aba7ed609c2b2821d576402))
* **sovereign-workplace-jitsi:** Create ConfigMaps for Prosody custom files ([b67f695](https://gitlab.souvap-univention.de/souvap/tooling/charts/sovereign-workplace-jitsi/commit/b67f6959fea52238031101dabe32135975535bc9))
* **sovereign-workplace-jitsi:** Update tags for Jitsi images ([de9d28c](https://gitlab.souvap-univention.de/souvap/tooling/charts/sovereign-workplace-jitsi/commit/de9d28ccc061289a9be94a573ff7f936b8f5f967))

## [1.1.3](https://gitlab.souvap-univention.de/souvap/tooling/charts/sovereign-workplace-jitsi/compare/v1.1.2...v1.1.3) (2023-08-13)


### Bug Fixes

* **sovereign-workplace-jitsi:** Fix Jibi Liveness prove ([96a0ec1](https://gitlab.souvap-univention.de/souvap/tooling/charts/sovereign-workplace-jitsi/commit/96a0ec1399541468c736a3bd9005ff69b1271575))

## [1.1.2](https://gitlab.souvap-univention.de/souvap/tooling/charts/sovereign-workplace-jitsi/compare/v1.1.1...v1.1.2) (2023-08-13)


### Bug Fixes

* **sovereign-workplace-jitsi:** Fix Jibri Auth and probes ([c2b37da](https://gitlab.souvap-univention.de/souvap/tooling/charts/sovereign-workplace-jitsi/commit/c2b37da361c48512818678c29652a5625d9c2e46))

## [1.1.1](https://gitlab.souvap-univention.de/souvap/tooling/charts/sovereign-workplace-jitsi/compare/v1.1.0...v1.1.1) (2023-08-13)


### Bug Fixes

* **sovereign-workplace-jitsi:** Fix NodePort bootstrap patch command ([64852f1](https://gitlab.souvap-univention.de/souvap/tooling/charts/sovereign-workplace-jitsi/commit/64852f1abc9fdc310460d51de63c92f05f9d2d34))

# [1.1.0](https://gitlab.souvap-univention.de/souvap/tooling/charts/sovereign-workplace-jitsi/compare/v1.0.1...v1.1.0) (2023-07-19)


### Features

* **sovereign-workplace-jitsi:** Add JVB advertising job ([4a43f4c](https://gitlab.souvap-univention.de/souvap/tooling/charts/sovereign-workplace-jitsi/commit/4a43f4c96fa0aaf98a0ecbfa2fa5ee6c83d68c74))

## [1.0.1](https://gitlab.souvap-univention.de/souvap/tooling/charts/sovereign-workplace-jitsi/compare/v1.0.0...v1.0.1) (2023-07-11)


### Bug Fixes

* **sovereign-workplace-jitsi:** Set default service externalTrafficPolicy ([12ad9b3](https://gitlab.souvap-univention.de/souvap/tooling/charts/sovereign-workplace-jitsi/commit/12ad9b306d9e18bb4a714ec0efaf85c3c1db49bb))

# 1.0.0 (2023-07-09)


### Features

* Initial commit ([20a4107](https://gitlab.souvap-univention.de/souvap/tooling/charts/sovereign-workplace-jitsi/commit/20a410755bd5abf44a645157b1e56bd3f38d8134))
