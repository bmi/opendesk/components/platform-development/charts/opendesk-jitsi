<!--
SPDX-FileCopyrightText: 2023 Bundesministerium des Innern und für Heimat, PG ZenDiS "Projektgruppe für Aufbau ZenDiS"
SPDX-License-Identifier: Apache-2.0
-->
# Sovereign Workplace Jitsi Helm Charts

A Helm chart for deploying Jitsi as separate scalable microservices.

## Prerequisites

Before you begin, ensure you have met the following requirements:

- Kubernetes 1.21+
- Helm 3.0.0+
- Optional: PV provisioner support in the underlying infrastructure

## Documentation

The documentation is placed in the README of each helm chart:

- [opendesk-jitsi](charts/opendesk-jitsi)

## License

This project uses the following license: Apache-2.0

## Copyright

Copyright © 2023 Bundesministerium des Innern und für Heimat, PG ZenDiS "Projektgruppe für Aufbau ZenDiS"
